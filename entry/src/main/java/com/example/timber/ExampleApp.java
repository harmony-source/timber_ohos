/*
 *    Copyright 2013 Jake Wharton
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.timber;

import ohos.aafwk.ability.AbilityPackage;
import ohos.hiviewdfx.HiLog;
import timber.log.Timber;


public class ExampleApp extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree(0x001f00));
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }
    private static class CrashReportingTree extends Timber.Tree {
        @Override protected void log(int priority, String tag,  String message, Throwable t) {
            if (priority == HiLog.INFO || priority == HiLog.DEBUG) {
                return;
            }

            FakeCrashLibrary.log(priority, tag, message);

            if (t != null) {
                if (priority == HiLog.ERROR) {
                    FakeCrashLibrary.logError(t);
                } else if (priority == HiLog.WARN) {
                    FakeCrashLibrary.logWarning(t);
                }
            }
        }
    }
}
