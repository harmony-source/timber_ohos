/*
 *    Copyright 2013 Jake Wharton
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.timber.slice;

import com.example.timber.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import timber.log.Timber;


public class MainAbilitySlice extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Timber.tag("LifeCycles");
        Timber.d("Timber 测试成功！！！");

        findComponentById(ResourceTable.Id_btn1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 按钮被点击
                // 按钮被按下后，需要执行的操作

                Timber.e("Timber.e 测试成功！！！");
                Timber.d("Timber.d 测试成功！！！");
                Timber.i("Timber.i 测试成功！！！");

                Timber.v("Timber.v 测试成功！！！");
                Timber.w("Timber.w 测试成功！！！");

                Timber.wtf("Timber.wtf测试成功！！！");
            }
        });
    }

}
